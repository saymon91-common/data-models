import { BuiltConverter, Converter, IDataModel } from './lib/types';

import { packageToStore } from './lib/packageToStore';
import { sourceToStore } from './lib/sourceToStore';

export { toType } from './lib/toType';

export { packageToStore } from './lib/packageToStore';
export { sourceToPackage } from './lib/sourceToPackage';
export { sourceToStore } from './lib/sourceToStore';
export { storeToPackage } from './lib/storeToPackage';

export const factory = <R, T>(converter: Converter<R, T>, model: IDataModel): BuiltConverter<R, T> => {
  if (converter === packageToStore || converter === sourceToStore) {
    return converter.bind(
      null,
      model.filter(({ storeKey }) => !!storeKey),
    );
  }

  return converter.bind(null, model);
};
