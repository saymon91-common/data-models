import { sourceToPackage } from '..';

import { IDataModel, INode } from '../lib/types';

const model: IDataModel = [
  {
    key: 'position.coordinates.0',
    source: 'lat',
    storeKey: 'latitude',
    type: 'number',
  } as INode<number>,
  {
    key: 'position.coordinates.1',
    source: 'lon',
    storeKey: 'longitude',
    type: 'number',
  } as INode<number>,
  {
    key: 'position.coordinates.2',
    source: 'height',
    storeKey: 'altitude',
    defaultValue: 0,
    type: 'number',
  } as INode<number>,
  {
    key: 'position.type',
    defaultValue: 'Point',
    type: 'string',
  } as INode<string>,
  {
    key: 'speed',
    source: 'speed',
    storeKey: 'speed',
    type: 'number',
  } as INode<number>,
];

test('SOURCE TO PACKET', () => {
  expect(sourceToPackage(model, { lat: 52.123234, lon: 32.434345, speed: 100 })).toEqual({
    position: {
      type: 'Point',
      coordinates: [52.123234, 32.434345, 0],
    },
    speed: 100,
  });
});
