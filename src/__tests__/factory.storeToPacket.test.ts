import { factory, storeToPackage } from '..';

import { IDataModel, INode } from '../lib/types';

const model: IDataModel = [
  {
    key: 'position.coordinates.0',
    source: 'lat',
    storeKey: 'latitude',
    type: 'number',
  } as INode<number>,
  {
    key: 'position.coordinates.1',
    source: 'lon',
    storeKey: 'longitude',
    type: 'number',
  } as INode<number>,
  {
    key: 'position.coordinates.2',
    source: 'height',
    storeKey: 'altitude',
    defaultValue: 0,
    type: 'number',
  } as INode<number>,
  {
    key: 'position.type',
    defaultValue: 'Point',
    type: 'string',
  } as INode<string>,
  {
    key: 'speed',
    source: 'speed',
    storeKey: 'speed',
    type: 'number',
  } as INode<number>,
];

test('FACTORY STORE TO PACKET', () => {
  expect(
    factory(storeToPackage, model)({ latitude: 52.123234, longitude: 32.434345, altitude: 0, speed: 100 }),
  ).toEqual({
    position: {
      type: 'Point',
      coordinates: [52.123234, 32.434345, 0],
    },
    speed: 100,
  });
});
