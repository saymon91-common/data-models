import { get, set } from 'lodash';

import { IDataModel } from './types';

import { toType } from './toType';

export const storeToPackage = <R, T>(model: IDataModel, data: R): T => {
  return model.reduce((res, { type, storeKey, key, defaultValue }): Partial<T> => {
    set(res, key, toType(type, storeKey ? get(data, storeKey, defaultValue) : defaultValue));
    return res;
  }, {}) as T;
};
