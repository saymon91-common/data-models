import { get, set } from 'lodash';

import { IDataModel } from './types';

import { toType } from './toType';

export const sourceToStore = <R, T>(model: IDataModel, data: R): T => {
  return model.reduce((res, { type, storeKey, source, defaultValue }): Partial<T> => {
    if (storeKey) {
      set(res, storeKey, toType(type, source ? get(data, source, defaultValue) : defaultValue));
    }
    return res;
  }, {}) as T;
};
