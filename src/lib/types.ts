export type Converter<R, T> = (model: IDataModel, data: R) => T;

export type BuiltConverter<R, T> = (data: R) => T;

export type DataTypes = 'string' | 'number' | 'boolean';

export interface INode<T extends number | string | boolean = any> {
  key: string;
  defaultValue: T | null;
  type: DataTypes;
  source?: string;
  storeKey?: string;
}

export type IDataModel = INode[];
