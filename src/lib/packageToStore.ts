import { get, set } from 'lodash';

import { IDataModel } from './types';

import { toType } from './toType';

export const packageToStore = <R, T>(model: IDataModel, data: R): T => {
  return model.reduce((res, { type, key, storeKey, defaultValue }): Partial<T> => {
    const value = key ? get(data, key, defaultValue) : defaultValue;
    if (storeKey) {
      set(res, storeKey, value && toType(type, value));
    }
    return res;
  }, {}) as T;
};
