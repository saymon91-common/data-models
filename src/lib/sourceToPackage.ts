import { get, set } from 'lodash';

import { IDataModel } from './types';

import { toType } from './toType';

export const sourceToPackage = <R, T>(model: IDataModel, data: R): T => {
  return model.reduce((res, { type, key, source, defaultValue }): Partial<T> => {
    set(res, key, toType(type, source ? get(data, source, defaultValue) : defaultValue));
    return res;
  }, {}) as T;
};
